package com.ticompany.strconsumer.listeners;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.ticompany.strconsumer.custom.StrconsumerCustomListner;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class StrConsumerListener {
	
	@StrconsumerCustomListner(groupId = "group-1")
	public void listener1(String message) {
		log.info("Receive1 message {}", message);
	}

	@StrconsumerCustomListner(groupId = "group-1")
	public void listener2(String message) {
		log.info("Receive2 message {}", message);
	}

	@KafkaListener(groupId = "group-2", topics = "str-topic", containerFactory = "interceptor")
	public void listener3(String message) {
		log.info("Receive3 message {}", message);
	}

}
