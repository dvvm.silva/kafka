package com.ticompany.paymentservice.resource.impl;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ticompany.paymentservice.model.Payment;
import com.ticompany.paymentservice.resource.PaymentResource;
import com.ticompany.paymentservice.service.PaymentService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api-payment")
public class PaymentResourceImpl implements PaymentResource{

	private final PaymentService paymentService;
	
	@Override
	public ResponseEntity<Payment> payment(Payment payment) {
		this.paymentService.sendPayment(payment);
		return ResponseEntity.status(HttpStatus.CREATED).body(payment);
	}

}
