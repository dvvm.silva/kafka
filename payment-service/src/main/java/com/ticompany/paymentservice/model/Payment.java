package com.ticompany.paymentservice.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Payment implements Serializable{

	private Long id;
	private Long IdUser;
	private Long idProduct;
	private String CardNumber;
	
}
