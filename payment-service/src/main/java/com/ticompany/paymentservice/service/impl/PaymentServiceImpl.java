package com.ticompany.paymentservice.service.impl;

import java.io.Serializable;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.ticompany.paymentservice.model.Payment;
import com.ticompany.paymentservice.service.PaymentService;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RequiredArgsConstructor
@Service
public class PaymentServiceImpl implements PaymentService{

	private final KafkaTemplate<String, Serializable> kafkaTemplate;
	
	@SneakyThrows
	@Override
	public void sendPayment(Payment payment) {
		log.info("PaymentServiceImpl ::: Recebi o pagamento {}", payment);
		Thread.sleep(1000);
		
		log.info("Registrando pagamento", payment);
		this.kafkaTemplate.send("payment-topic", payment);
	}

}
