package com.ticompany.paymentservice.service;

import com.ticompany.paymentservice.model.Payment;

public interface PaymentService {

	void sendPayment(Payment payment);
}
