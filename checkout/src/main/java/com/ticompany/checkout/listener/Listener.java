package com.ticompany.checkout.listener;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.ticompany.checkout.model.Payment;

import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class Listener {

	@SneakyThrows
	@KafkaListener(topics = "paymente-topic", groupId = "anti-fraud", containerFactory = "concurrentKafkaListenerContainerFactory")
	public void antiFraud(@Payload Payment payment) {


		log.info("recebi o pagamento {}", payment.toString());
		Thread.sleep(9000);		
		log.info("Validando fraude");
		Thread.sleep(2000);
		
		log.info("Compra aprovada");
		Thread.sleep(2000);
	}
	
	@SneakyThrows
	@KafkaListener(topics = "paymente-topic", groupId = "pdf-group-", containerFactory = "concurrentKafkaListenerContainerFactory")
	public void pdfGenerator(@Payload Payment payment) {
		log.info("Gerando PDF do produto de id {}", payment.getId());
		Thread.sleep(2000);
	}
	
	@SneakyThrows
	@KafkaListener(topics = "paymente-topic", groupId = "email-group-", containerFactory = "concurrentKafkaListenerContainerFactory")
	public void sendEmail() {
		log.info(" Enviando emailF");
		Thread.sleep(2000);
	}

}
