package com.ticompany.strproducer.service.impl;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.ticompany.strproducer.service.ProducerService;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RequiredArgsConstructor
@Service
public class ProducerServiceImpl implements ProducerService{

	private final KafkaTemplate<String, String> kafkaTemplate;

	@Override
	public void sendMessage(String message) {
		this.kafkaTemplate.send("str-topic", message).addCallback(
				successCallback -> {
					log.info("Message send successfully {}", message);
					log.info("Partition {}, offset {}", successCallback.getRecordMetadata().partition(),
							successCallback.getRecordMetadata().offset());
				},
				FailureCallback -> {
					log.debug("Failed to send message StackTracer {}", FailureCallback.fillInStackTrace());
					log.debug(" Cause{}", FailureCallback.getCause());
				});

	}
}
