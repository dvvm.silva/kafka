package com.ticompany.strproducer.service;

public interface ProducerService {

	public void sendMessage(String message);
}
